import {assert} from 'chai';
import {GildedRose} from '../app/gilded-rose';
import {Item} from "../app/item";
import {SpecialItems} from "../app/special-items";


const getUpdatedItem = (item: Item) => {
    const gildedRose = new GildedRose([item]);
    const items = gildedRose.updateQuality();
    return items[0];
};

describe('Gilded Rose', () => {

    describe('Quality and sellIn should be reduced by 1 for a regular item after 1 update ', () => {
        it('Normal item test ', () => {
            const updatedItem = getUpdatedItem(new Item('x', 10, 10));
            assert.equal(updatedItem.quality, 9);
            assert.equal(updatedItem.sellIn, 9);
        });
    });

    describe('Quality should decreases twice as fast when sell date has passed', () => {
        it('Update with sellIn at 0 should decrease quality by 2', () => {
            const updatedItem = getUpdatedItem(new Item('x', 0, 10));
            assert.equal(updatedItem.sellIn, -1);
            assert.equal(updatedItem.quality, 8);
        });

        it('Update with sellIn at less than 0 should decrease quality by 2 as well', () => {
            const updatedItem = getUpdatedItem(new Item('x', -3, 10));
            assert.equal(updatedItem.sellIn, -4);
            assert.equal(updatedItem.quality, 8);
        });

        it('Update with sellIn at 1 should decrease quality by 1', () => {
            const updatedItem = getUpdatedItem(new Item('x', 1, 10));
            assert.equal(updatedItem.sellIn, 0);
            assert.equal(updatedItem.quality, 9);
        });
    });

    describe('Quality should never be negative', () => {
        it('Quality should never be negative', () => {
            const updatedItem = getUpdatedItem(new Item('x', 10, 0));
            assert.equal(updatedItem.quality, 0);
        });

        it('Quality should never be negative 2', () => {
            const updatedItem = getUpdatedItem(new Item('x', -5, 0));
            assert.equal(updatedItem.quality, 0);
        });
    });

    describe('Aged brie gains quality instead of losing it and has a cap of 50', () => {
        it('Quality increase test', () => {
            const updatedItem = getUpdatedItem(new Item(SpecialItems.AGED_BRIE, 10, 10));
            assert.equal(updatedItem.quality, 11);
        });

        it('Quality "faster increase" test', () => {
            const updatedItem = getUpdatedItem(new Item(SpecialItems.AGED_BRIE, -5, 10));
            assert.equal(updatedItem.quality, 12);
        });

        it('Quality cant increase  beyond 50', () => {
            const updatedItem = getUpdatedItem(new Item(SpecialItems.AGED_BRIE, 10, 50));
            assert.equal(updatedItem.quality, 50);
        });
    });

    describe('Sulfuras quality doesnt change and it doesnt need to be sold', () => {
        it('Quality and sellIn remains the same', () => {
            const updatedItem = getUpdatedItem(new Item(SpecialItems.SULFURAS, 10, 80));
            assert.equal(updatedItem.sellIn, 10);
            assert.equal(updatedItem.quality, 80);
        });

        it('Quality and sellIn remains the same 2', () => {
            const updatedItem = getUpdatedItem(new Item(SpecialItems.SULFURAS, -5, 80));
            assert.equal(updatedItem.sellIn, -5);
            assert.equal(updatedItem.quality, 80);
        });
    });

    describe('Backstage passes gains quality as its sellIn decreases, but in a more complex way', () => {
        describe('Its quality increases by 1 if its sellIn is greater than 10', () => {
            it('Quality should increase by 1 with a random sellIn that is greater than 10', () => {
                const updatedItem = getUpdatedItem(new Item(SpecialItems.BACKSTAGE_PASSES, 30, 20));
                assert.equal(updatedItem.quality, 21);
            });

            it('Quality should increase by 1 with a sellIn of 11', () => {
                const updatedItem = getUpdatedItem(new Item(SpecialItems.BACKSTAGE_PASSES, 11, 20));
                assert.equal(updatedItem.quality, 21);
            });

            it('Quality cant increase  beyond 50', () => {
                const updatedItem = getUpdatedItem(new Item(SpecialItems.BACKSTAGE_PASSES, 15, 50));
                assert.equal(updatedItem.quality, 50);
            });
        });

        describe('Its quality increases by 2 if  6 <= sellIn <= 10', () => {
            it('Quality should increase by 2 with a sellIn of 10', () => {
                const updatedItem = getUpdatedItem(new Item(SpecialItems.BACKSTAGE_PASSES, 10, 20));
                assert.equal(updatedItem.quality, 22);
            });

            it('Quality should increase by 2 with a sellIn of 6', () => {
                const updatedItem = getUpdatedItem(new Item(SpecialItems.BACKSTAGE_PASSES, 6, 20));
                assert.equal(updatedItem.quality, 22);
            });

            it('Quality cant increase  beyond 50', () => {
                const updatedItem = getUpdatedItem(new Item(SpecialItems.BACKSTAGE_PASSES, 8, 50));
                assert.equal(updatedItem.quality, 50);
            });
        });

        describe('Its quality increases by 3 if 1 <= sellIn <= 5', () => {
            it('Quality should increase by 3 with a sellIn of 5', () => {
                const updatedItem = getUpdatedItem(new Item(SpecialItems.BACKSTAGE_PASSES, 5, 20));
                assert.equal(updatedItem.quality, 23);
            });

            it('Quality should increase by 2 with a sellIn of 6', () => {
                const updatedItem = getUpdatedItem(new Item(SpecialItems.BACKSTAGE_PASSES, 1, 20));
                assert.equal(updatedItem.quality, 23);
            });

            it('Quality cant increase  beyond 50', () => {
                const updatedItem = getUpdatedItem(new Item(SpecialItems.BACKSTAGE_PASSES, 3, 50));
                assert.equal(updatedItem.quality, 50);
            });
        });

        describe('Its quality drops to 0 on the last day (sellIn == 0)', () => {
            it('Quality should increase by 3 with a sellIn of 5', () => {
                const updatedItem = getUpdatedItem(new Item(SpecialItems.BACKSTAGE_PASSES, 0, 20));
                assert.equal(updatedItem.quality, 0);
            });
        });

        describe('Conjured item degrades twice as fast as a normal item',() => {
            describe('Conjured item loses 2 quality per update at sellIn > 0',() => {
                it('Quality should decrease by 2 with a sellIn of 10', () => {
                    const updatedItem = getUpdatedItem(new Item('Conjured chainmail', 10, 20));
                    assert.equal(updatedItem.quality, 18);
                });

                it('Quality should decrease by 2 with a sellIn of 1', () => {
                    const updatedItem = getUpdatedItem(new Item('Conjured chainmail', 1, 20));
                    assert.equal(updatedItem.quality, 18);
                });
            });

            describe('Conjured item loses 4 quality per update at sellIn <= 0',() => {
                it('Quality should decrease by 4 with a sellIn of 0', () => {
                    const updatedItem = getUpdatedItem(new Item('Conjured chainmail', 0, 20));
                    assert.equal(updatedItem.quality, 16);
                });

                it('Quality should decrease by 4 with a sellIn of -5', () => {
                    const updatedItem = getUpdatedItem(new Item('Conjured chainmail', -5, 20));
                    assert.equal(updatedItem.quality, 16);
                });
            });
        })
    });
});