import {Item} from "./item";
import {SpecialItems} from "./special-items";
import {ItemPrefixes} from "./item-prefixes";

export function updateItem(item: Item) {
    if (item.name === SpecialItems.SULFURAS) {
        return;
    }
    --item.sellIn;
    switch (item.name) {
        case SpecialItems.AGED_BRIE:
            updateAgedBrie(item);
            break;
        case SpecialItems.BACKSTAGE_PASSES:
            updateBackstagePasses(item);
            break;
        default:
            if (hasPrefix(item, ItemPrefixes.CONJURED)) {
                updateConjuredItem(item);
            } else {
                updateNormalItem(item);
            }
    }
}

function updateAgedBrie(item: Item) {
    increaseQuality(item, item.sellIn < 0 ? 2 : 1);
}

function updateBackstagePasses(item: Item) {
    if (item.sellIn < 0) {
        item.quality = 0;
    } else if (item.sellIn < 5) {
        increaseQuality(item, 3);
    } else if (item.sellIn < 10) {
        increaseQuality(item, 2);
    } else {
        increaseQuality(item);
    }
}

function updateNormalItem(item: Item) {
    decreaseQuality(item, item.sellIn < 0 ? 2 : 1);
}

function updateConjuredItem(item: Item) {
    decreaseQuality(item, item.sellIn < 0 ? 4 : 2);
}

function increaseQuality(item: Item, n: number = 1) {
    item.quality = Math.min(item.quality + n, 50);
}

function decreaseQuality(item: Item, n: number = 1) {
    item.quality = Math.max(item.quality - n, 0);
}

function hasPrefix(item: Item, prefix: string) {
    return item.name.startsWith(prefix);
}