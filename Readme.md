**Installation**
1. Clone the repository
2. `npm install`

**Execution**

`npm start` to clean&compile, run tests and the main code

`npm run clean-and-compile` to clean and compile TS to JS

`npm run test` to only run the tests

`npm run exec` to only execute the "main" code